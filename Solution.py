class Solution:
	def strStr(self, source, target):
		if(source is None or target is None):
			return -1
		i,j=0,0
		for i in range(0,len(source)-len(target)+1):
			for j in range(0,len(target)):
				if(source[i+j]!=target[j]):
					break
			if(j==len(target)-1):
				return i
		return -10

if __name__ == '__main__':
	s = Solution()		
	source="abcdabcdefg"
	target="bcd"
	print s.strStr(source,target)

# if __name__=='__main__':
